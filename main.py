from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext
import toml
import os
import time

config = toml.load("config.toml")
TOKEN = config["token"]["value"]
AUTHORIZED_USERS = set(config["authorized_users"]["ids"])

print(AUTHORIZED_USERS)

def is_user_authorized(user_id):
    """Check if the user is authorized to use the bot."""
    if user_id in AUTHORIZED_USERS:
        return True
    else:
        print(f"User {user_id} is not authorized.")
        return False

def pong(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    if not is_user_authorized(update.message.chat_id):
        update.message.reply_text('You are not authorized to use this bot.')
        return
    update.message.reply_text('Pong!')

def handle_screenshot(update: Update, context: CallbackContext) -> None:
    """Handle screenshot command."""
    if not is_user_authorized(update.message.chat_id):
        update.message.reply_text('You are not authorized to use this bot.')
        return
    print("Got screenshot request!")
    try:
        update.message.reply_text('Taking a screenshot...')
        os.system('DISPLAY=:1 xdotool mousemove 1000 500')
        time.sleep(0.5)
        # TODO if screenshot fails it will still send the old screenshot sometimes
        screenshot_filename = '/tmp/screenshot.png'
        os.system('DISPLAY=:1 scrot --silent --overwrite -f "{}"'.format(screenshot_filename))
        with open(screenshot_filename, 'rb') as photo:
            update.message.reply_photo(photo=photo)
    except Exception as e:
        update.message.reply_text(f"Failed to take screenshot: {e}")

def handle_voice(update: Update, context: CallbackContext) -> None:
    """Handle incoming voice messages."""
    if not is_user_authorized(update.message.chat_id):
        update.message.reply_text('You are not authorized to use this bot.')
        return
    print("Got voice file!")
    audio_file = context.bot.getFile(update.message.voice.file_id)
    file_path = f"/tmp/{update.message.voice.file_id}.mp3"
    print(f"File path: {file_path}")
    audio_file.download(file_path)
    os.system(f'ffplay -nodisp -autoexit "{file_path}"')

def handle_shutdown(update: Update, context: CallbackContext) -> None:
    """Handle shutdown command."""
    if not is_user_authorized(update.message.chat_id):
        update.message.reply_text('You are not authorized to use this bot.')
        return
    print("Got shutdown request!")
    update.message.reply_text('Shutting down...')
    time.sleep(3)
    os.system('sudo shutdown -h now')

def main() -> None:
    """Start the bot."""
    updater = Updater(TOKEN, use_context=True)

    dp = updater.dispatcher

    for user_id in AUTHORIZED_USERS:
        try:
            # TODO Make this configurable in the toml file
            continue
            updater.bot.send_message(chat_id=user_id, text="Hi! I am online.")
        except Exception as e:
            print(f"Failed to send message to {user_id}: {e}")

    dp.add_handler(CommandHandler("ping", pong))
    dp.add_handler(CommandHandler("screenshot", handle_screenshot))
    dp.add_handler(CommandHandler("shutdown", handle_shutdown))
    dp.add_handler(MessageHandler(Filters.voice, handle_voice))

    # Start the bot
    print("Starting bot...")
    updater.start_polling()

    updater.idle()

if __name__ == '__main__':
    main()
