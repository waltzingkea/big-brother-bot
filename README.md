# big-brother-bot

`sudo apt install ffmpeg` Needed for ffplay to play voice messages.
`cd ~/.local`
`git clone https://gitlab.com/waltzingkea/big-brother-bot.git`
`cd big-brother-bot`
`cp config_TEMPlATE.toml config.toml`
Fill out config file with telegram bot API token and approved user IDs
`mkdir -p ~/.config/systemd/user`
`sed "s/your_username/$USER/g" big-brother-bot.service.template > ~/.config/systemd/user/big-brother-bot.service`
`systemctl --user enable big-brother-bot.service`
`systemctl --user start big-brother-bot.service`
`systemctl --user status big-brother-bot.service`
